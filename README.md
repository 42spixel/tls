# **Tls**

Nginx server to assign [Letsencrypt](https://letsencrypt.org/) TLS certs to each Docker container/service.

### **Credits**

+ [jrcs/docker-letsencrypt-nginx-proxy-companion](https://github.com/JrCs/docker-letsencrypt-nginx-proxy-companion)

### **MIT License**

This work is licensed under the terms of **[MIT License](https://bitbucket.org/42spixel/tls/src/master/LICENSE.md)**.
